FROM openjdk:8-jdk-alpine

WORKDIR /app

COPY target/JpwIntegrator-0.0.1-SNAPSHOT.jar /app/jpw-service-integrator.jar

ENTRYPOINT ["java", "-jar", "jpw-service-integrator.jar"]