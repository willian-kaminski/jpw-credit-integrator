package com.br.jpw.JpwIntegrator.domain;

import lombok.Data;

@Data
public class Endereco {

    private String cep;
    private String logradouro;
    private String bairro;
    private String cidade;
    private String uf;
    private String numero;
    private String complemento;

}
