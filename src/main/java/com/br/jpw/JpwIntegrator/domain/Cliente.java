package com.br.jpw.JpwIntegrator.domain;

import com.br.jpw.JpwIntegrator.enuns.Sexo;
import lombok.Data;
import java.util.Date;

@Data
public class Cliente {

    private Long idCliente;
    private String nome;
    private Sexo sexo;
    private String rg;
    private String cpf;
    private Double renda;
    private String email;
    private Date dataNascimento;
    private String telefone;
    private Endereco endereco;

}
