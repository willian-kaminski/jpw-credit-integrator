package com.br.jpw.JpwIntegrator.controller.dto;

import com.br.jpw.JpwIntegrator.domain.Endereco;
import com.br.jpw.JpwIntegrator.enuns.Sexo;
import lombok.Data;
import java.util.Date;

@Data
public class ClienteResponseDTO {

    private Long id;
    private String nome;
    private Sexo sexo;
    private String rg;
    private String cpf;
    private Double renda;
    private String email;
    private Date dataNascimento;
    private String telefone;
    private Endereco endereco;

}
