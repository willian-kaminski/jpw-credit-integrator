package com.br.jpw.JpwIntegrator.controller;

import com.br.jpw.JpwIntegrator.util.PageFeign;
import com.br.jpw.JpwIntegrator.controller.dto.ClienteResponseDTO;
import com.br.jpw.JpwIntegrator.controller.form.ClienteForm;
import com.br.jpw.JpwIntegrator.service.implementation.ClienteClientService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import org.springframework.data.domain.Pageable;

@RestController
@AllArgsConstructor
@RequestMapping("api/v1/admin/cliente")
@Api(tags = "cliente (Clientes)", description = "Localização: Cadastros >> Clientes")
public class ClienteController {

    private ClienteClientService clienteClientService;

    @PostMapping
    @ApiOperation(value = "Registra um novo cliente na base de dados")
    public ResponseEntity<ClienteResponseDTO> cadastrarCliente(@RequestBody @Valid ClienteForm clienteForm){
        return ResponseEntity
                .status(HttpStatus.CREATED)
                .body(clienteClientService.cadastrarCliente(clienteForm));
    }

    @GetMapping
    @ApiOperation(value = "Retorna uma lista paginada de clientes")
    public ResponseEntity<PageFeign<ClienteResponseDTO>> listarTodos(Pageable pageable){
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(clienteClientService.listarClientes(pageable));
    }

}
