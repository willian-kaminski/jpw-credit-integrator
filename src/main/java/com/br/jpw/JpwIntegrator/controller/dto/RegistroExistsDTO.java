package com.br.jpw.JpwIntegrator.controller.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class RegistroExistsDTO {

    private String cpf;
    private Boolean existe;

}
