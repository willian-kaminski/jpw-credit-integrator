package com.br.jpw.JpwIntegrator.controller.form;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class VendaForm {

    @NotNull
    private String clienteCPF;

    @NotNull
    private Long idCartao;

    @NotNull
    private Long idProduto;


}
