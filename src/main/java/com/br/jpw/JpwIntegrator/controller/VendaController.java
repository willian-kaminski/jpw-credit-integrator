package com.br.jpw.JpwIntegrator.controller;

import com.br.jpw.JpwIntegrator.controller.dto.VendaResponseDTO;
import com.br.jpw.JpwIntegrator.controller.form.VendaForm;
import com.br.jpw.JpwIntegrator.service.implementation.VendaClientService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@AllArgsConstructor
@RequestMapping("api/v1/fat/venda")
@Api(tags = "venda (Vendas)", description = "Localização: Executando >> Venda")
public class VendaController {

    private VendaClientService vendaClientService;

    @PostMapping
    @ApiOperation(value = "Registra uma nova venda na base de dados")
    public ResponseEntity<VendaResponseDTO> cadastrarVenda(@RequestBody @Valid VendaForm vendaForm){
        return ResponseEntity
                .status(HttpStatus.CREATED)
                .body(vendaClientService.cadastrarVenda(vendaForm));
    }

    @GetMapping
    @ApiOperation(value = "Restorna uma lista de vendas de nossa base de dados")
    public ResponseEntity<List<VendaResponseDTO>> listarTodos(){
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(vendaClientService.listarTodos());
    }

}
