package com.br.jpw.JpwIntegrator.controller.dto;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class VendaResponseDTO {

    private Long id;
    private Long idCliente;
    private Long idCartao;
    private Long idProduto;
    private LocalDateTime dataVenda;

}
