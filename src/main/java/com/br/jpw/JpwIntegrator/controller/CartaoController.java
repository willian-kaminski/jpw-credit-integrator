package com.br.jpw.JpwIntegrator.controller;

import com.br.jpw.JpwIntegrator.controller.dto.CartaoResponseDTO;
import com.br.jpw.JpwIntegrator.controller.form.CartaoForm;
import com.br.jpw.JpwIntegrator.service.implementation.CartaoClientService;
import com.br.jpw.JpwIntegrator.util.PageFeign;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@AllArgsConstructor
@RequestMapping("api/v1/admin/cartao")
@Api(tags = "cartao (Cartões)", description = "Localização: Cadastros >> Cartões")
public class CartaoController {

    private CartaoClientService cartaoClientService;

    @PostMapping
    @ApiOperation(value = "Registra um novo cartão na base de dados")
    public ResponseEntity<CartaoResponseDTO> cadastrarCartao(@RequestBody @Valid CartaoForm cartaoForm){
        return ResponseEntity
                .status(HttpStatus.CREATED)
                .body(cartaoClientService.cadastrarCartao(cartaoForm));
    }

    @GetMapping
    @ApiOperation(value = "Retorna uma lista paginada de cartões")
    public ResponseEntity<PageFeign<CartaoResponseDTO>> listarTodos(Pageable pageable){
        return ResponseEntity
                .status(HttpStatus.CREATED)
                .body(cartaoClientService.listarCartoes(pageable));
    }

}
