package com.br.jpw.JpwIntegrator.controller.form;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;

@Data
public class CartaoForm {

    @NotNull
    private Long idCliente;

    @NotNull
    @Size(min = 10)
    private String nome;

    @NotNull
    private String rg;

    @NotNull
    @Size(min = 11, max = 11)
    private String cpf;

    @NotNull
    private Double renda;

    @NotNull
    @Email
    private String email;

    @NotNull
    @JsonFormat(pattern="YYYY-MM-dd")
    private Date dataNascimento;

    @NotNull
    private String telefone;

}
