package com.br.jpw.JpwIntegrator.controller.dto;

import com.br.jpw.JpwIntegrator.enuns.Categoria;
import com.br.jpw.JpwIntegrator.domain.Cliente;
import lombok.Data;
import java.util.Date;

@Data
public class CartaoResponseDTO {

    private Long id;
    private Cliente cliente;
    private String codigoCartao;
    private Categoria categoria;
    private Double limite;
    private Date dataValidade;
    private boolean isAtivo;

}
