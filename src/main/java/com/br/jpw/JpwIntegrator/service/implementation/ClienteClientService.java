package com.br.jpw.JpwIntegrator.service.implementation;

import com.br.jpw.JpwIntegrator.service.ClienteService;
import com.br.jpw.JpwIntegrator.util.PageFeign;
import com.br.jpw.JpwIntegrator.controller.dto.ClienteResponseDTO;
import com.br.jpw.JpwIntegrator.controller.form.ClienteForm;
import com.br.jpw.JpwIntegrator.validation.ValidaCliente;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Pageable;

@Service
@AllArgsConstructor
public class ClienteClientService {

    private ClienteService clienteService;
    private ValidaCliente validaCliente;

    public ClienteResponseDTO cadastrarCliente(ClienteForm clienteForm){
        validaCliente.clienteOrClienteAlreadyExists(clienteService, clienteForm.getCpf());
        return clienteService.cadastrar(clienteForm);
    }

    public PageFeign<ClienteResponseDTO> listarClientes(Pageable pageable){
        return clienteService.listarTodos(pageable);
    }

}
