package com.br.jpw.JpwIntegrator.service;

import com.br.jpw.JpwIntegrator.controller.dto.CartaoResponseDTO;
import com.br.jpw.JpwIntegrator.controller.dto.ClienteResponseDTO;
import com.br.jpw.JpwIntegrator.controller.dto.RegistroExistsDTO;
import com.br.jpw.JpwIntegrator.controller.form.CartaoForm;
import com.br.jpw.JpwIntegrator.util.PageFeign;
import feign.Headers;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@FeignClient(name = "CartaoResponseDTO", url = "http://card:8080/api/v1")
public interface CartaoService {

    @RequestMapping(value = "/cartao", method = RequestMethod.POST)
    @Headers({"Accept: application/json", "Content-Type: application/json"})
    CartaoResponseDTO cadastrar(@RequestBody CartaoForm cartaoForm);

    @RequestMapping(value = "/cartao/cpf/{cpf}", method = RequestMethod.GET)
    RegistroExistsDTO verificarSeClienteJaPossuiCartaoAtivo(@PathVariable String cpf);

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    CartaoResponseDTO retornarCartaoById(@PathVariable Long id);

    @RequestMapping(value = "/cartao", method = RequestMethod.GET)
    PageFeign<CartaoResponseDTO> listarTodos(Pageable pageable);

}
