package com.br.jpw.JpwIntegrator.service;

import com.br.jpw.JpwIntegrator.controller.dto.VendaResponseDTO;
import com.br.jpw.JpwIntegrator.controller.form.VendaForm;
import feign.Headers;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

@FeignClient(name = "ClienteResponseDTO", url = "http://sale:8080/api/v1")
public interface VendaService {

    @RequestMapping(value = "/venda", method = RequestMethod.POST)
    @Headers({"Accept: application/json", "Content-Type: application/json"})
    VendaResponseDTO cadastrar(@RequestBody VendaForm vendaForm);

    @RequestMapping(value = "/venda", method = RequestMethod.GET)
    @Headers({"Accept: application/json", "Content-Type: application/json"})
    List<VendaResponseDTO> listarTodos();

}
