package com.br.jpw.JpwIntegrator.service.implementation;

import com.br.jpw.JpwIntegrator.controller.dto.CartaoResponseDTO;
import com.br.jpw.JpwIntegrator.controller.dto.ClienteResponseDTO;
import com.br.jpw.JpwIntegrator.controller.form.CartaoForm;
import com.br.jpw.JpwIntegrator.service.CartaoService;
import com.br.jpw.JpwIntegrator.util.PageFeign;
import com.br.jpw.JpwIntegrator.validation.ValidaCartao;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class CartaoClientService {

    private CartaoService cartaoService;
    private ValidaCartao validaCartao;

    public CartaoResponseDTO cadastrarCartao(CartaoForm cartaoForm){
        validaCartao.cartaoOrCartaoAlreadyExists(cartaoService, cartaoForm.getCpf());
        return cartaoService.cadastrar(cartaoForm);
    }

    public PageFeign<CartaoResponseDTO> listarCartoes(Pageable pageable){
        return cartaoService.listarTodos(pageable);
    }

}
