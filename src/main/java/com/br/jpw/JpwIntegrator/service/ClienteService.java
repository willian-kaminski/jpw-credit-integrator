package com.br.jpw.JpwIntegrator.service;

import com.br.jpw.JpwIntegrator.util.PageFeign;
import com.br.jpw.JpwIntegrator.controller.dto.ClienteResponseDTO;
import com.br.jpw.JpwIntegrator.controller.dto.RegistroExistsDTO;
import com.br.jpw.JpwIntegrator.controller.form.ClienteForm;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.cloud.openfeign.FeignClient;
import feign.Headers;

@FeignClient(name = "ClienteResponseDTO", url = "http://client:8080/api/v1")
public interface ClienteService {

    @RequestMapping(value = "/cliente", method = RequestMethod.POST)
    @Headers({"Accept: application/json", "Content-Type: application/json"})
    ClienteResponseDTO cadastrar(@RequestBody ClienteForm clienteForm);

    @RequestMapping(value = "/cliente/cpf/{cpf}", method = RequestMethod.GET)
    RegistroExistsDTO verificarSeClienteJaEstaCadastrado(@PathVariable String cpf);

    @RequestMapping(value = "/cliente", method = RequestMethod.GET)
    PageFeign<ClienteResponseDTO> listarTodos(Pageable pageable);

}
