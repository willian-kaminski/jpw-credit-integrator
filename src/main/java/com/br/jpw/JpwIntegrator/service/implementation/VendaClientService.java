package com.br.jpw.JpwIntegrator.service.implementation;

import com.br.jpw.JpwIntegrator.controller.dto.CartaoResponseDTO;
import com.br.jpw.JpwIntegrator.controller.dto.VendaResponseDTO;
import com.br.jpw.JpwIntegrator.controller.form.VendaForm;
import com.br.jpw.JpwIntegrator.service.CartaoService;
import com.br.jpw.JpwIntegrator.service.ClienteService;
import com.br.jpw.JpwIntegrator.service.VendaService;
import com.br.jpw.JpwIntegrator.validation.ValidaCartao;
import com.br.jpw.JpwIntegrator.validation.ValidaCliente;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class VendaClientService {

    private ValidaCartao validaCartao;
    private ValidaCliente validaCliente;

    private VendaService vendaService;
    private ClienteService clienteService;
    private CartaoService cartaoService;

    public VendaResponseDTO cadastrarVenda(VendaForm vendaForm){
        validaCliente.clienteOrClienteAlreadyExists(clienteService, vendaForm.getClienteCPF());
        validaCartao.cartaoOrCartaoAlreadyExists(cartaoService, vendaForm.getIdCartao());
        return vendaService.cadastrar(vendaForm);
    }

    public List<VendaResponseDTO> listarTodos(){
        return vendaService.listarTodos();
    }

}
