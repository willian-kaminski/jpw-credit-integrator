package com.br.jpw.JpwIntegrator.util;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Getter
@Setter
public class PageFeign<T> implements Serializable {

    private static final long serialVersionUID = 1L;

    private List<T> content = new ArrayList<>();
    private boolean last;
    private boolean first;
    private boolean empty;
    private int totalPages;
    private int totalElements;
    private int numberOfElements;
    private int size;
    private int number;

    private Map<String, Object> pageable = new HashMap<>();
    private Map<String, Boolean> sort = new HashMap<>();

}
