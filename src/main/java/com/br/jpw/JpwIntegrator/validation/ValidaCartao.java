package com.br.jpw.JpwIntegrator.validation;

import com.br.jpw.JpwIntegrator.controller.dto.CartaoResponseDTO;
import com.br.jpw.JpwIntegrator.controller.dto.RegistroExistsDTO;
import com.br.jpw.JpwIntegrator.service.CartaoService;
import org.springframework.stereotype.Component;

@Component
public class ValidaCartao {

    public void cartaoOrCartaoAlreadyExists(CartaoService cartaoService, String cpf){
        RegistroExistsDTO registroExistsDTO = cartaoService.verificarSeClienteJaPossuiCartaoAtivo(cpf);
        if(registroExistsDTO.getExiste()){
            throw new RuntimeException(String.format("O cliente com o CPF: %s já possui um cartão ativo.", cpf));
        }
    }

    public CartaoResponseDTO cartaoOrCartaoAlreadyExists(CartaoService cartaoService, Long id){
        CartaoResponseDTO cartaoResponseDTO = cartaoService.retornarCartaoById(id);
        if(cartaoResponseDTO == null){
            throw new RuntimeException(String.format("Não foi encontrado cartão com o id '%s' ", id));
        }

        return cartaoResponseDTO;
    }

}
