package com.br.jpw.JpwIntegrator.validation;

import com.br.jpw.JpwIntegrator.controller.dto.RegistroExistsDTO;
import com.br.jpw.JpwIntegrator.service.ClienteService;
import org.springframework.stereotype.Component;

@Component
public class ValidaCliente {

    public void clienteOrClienteAlreadyExists(ClienteService clienteService, String cpf){
        RegistroExistsDTO registroExistsDTO = clienteService.verificarSeClienteJaEstaCadastrado(cpf);
        if(registroExistsDTO.getExiste()){
            throw new RuntimeException(String.format("Ja ha um cliente cadastrado com o CPF: %s", cpf));
        }
    }

}
